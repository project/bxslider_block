About
=====
Integrates the bxSlider library into Drupal.

About bxSlider
----------------

Library available at https://github.com/wandoledzep/bxslider-4

- Simple, semantic markup
- Supported in all major browsers
- Horizontal/vertical slide and fade animations
- Multiple slider support, Callback API, and more
- Hardware accelerated touch swipe support
- Custom navigation options
- Use any html elements in the slides
- Built for beginners and pros, alike
- Free to use under the GPLv2+ license

Installation
============

Dependencies
------------

- [Libraries API 2.x](http://drupal.org/project/libraries)
- [bxSlider Library](https://github.com/wandoledzep/bxslider-4)

Tasks
-----

1. Download the bxSlider library from
https://github.com/wandoledzep/bxslider-4
(To use Composer instead, see instructions in the Composer section below)
2. Unzip the file and rename the folder to "bxslider" (pay attention to the
case of the letters)
3. Put the folder in one of the following places relative to drupal root.
    - libraries
    - profiles/PROFILE-NAME/libraries
    - sites/all/libraries
    - sites/SITE-NAME/libraries
4. The following files are required (last file is required for javascript
debugging)
    - src/js/jquery.bxslider.js
    - src/css/jquery.bxslider.css
5. Ensure you have a valid path similar to this one for all files
    - Ex: libraries/bxslider/jquery.bxslider.min.js

That's it!


Usage
======

External Links
==============

- [Wiki Documentation for bxslider]
(http://bxslider.com/)
