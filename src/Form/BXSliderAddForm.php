<?php

namespace Drupal\bxslider_block\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Asset\LibraryDiscovery;
use Drupal\Core\Cache\CacheBackendInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\bxslider_block\BXSlider;
use Drupal\bxslider_block\BXSliderModel;
use Drupal\bxslider_block\Process\SliderRender;

/**
 * Class BXSliderAddForm.
 *
 * @package Drupal\bxslider_block\Form
 */
class BXSliderAddForm extends ConfigFormBase {

  protected $slider;

  /**
   * The library discovery service.
   *
   * @var \Drupal\Core\Asset\LibraryDiscovery
   */
  private $libraryDiscovery;
  private $cacheBackendInterface;

  /**
   * Constructs a new bXSliderFormSettings.
   */
  public function __construct(ConfigFactoryInterface $configFactory, LibraryDiscovery $libraryDiscovery, CacheBackendInterface $cacheBackendInterface) {
    parent::__construct($configFactory);
    $this->libraryDiscovery = $libraryDiscovery;
    $this->cacheBackendInterface = $cacheBackendInterface;
  }

  /**
   * Use Symfony's ContainerInterface to declare dependency for constructor.
   *
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('library.discovery'),
      $container->get('cache.discovery')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bxslider_add';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['bxslider.add'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $machine_name = NULL) {
    if ($machine_name == NULL) {
      $slider = new BXSlider();
    }
    else {
      $slider = new BXSlider();
      $slider->getDataSlider($machine_name);
    }

    $this->slider = $slider;
    $settings = $slider->settings;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $slider->title,
      '#description' => $this->t("Label for the Mega Slider."),
      '#required' => TRUE,
      '#disabled' => !empty($slider->machineName),
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $slider->machineName,
      '#machine_name' => [
        'exists' => '\Drupal\bxslider_block\BXSlider::isSlider',
      ],
      '#disabled' => !empty($slider->machineName),
    ];
    $form['is_new'] = [
      '#type' => 'hidden',
      '#value' => empty($slider->machineName),
    ];

    $form['general'] = [
      '#type' => 'details',
      '#title' => $this->t('General'),
      '#open' => TRUE,
    ];
    $form['general']['mode'] = [
      '#type' => 'select',
      '#options' => [
        'horizontal' => $this->t('Horizontal'),
        'vertical' => $this->t('Vertical'),
        'fade' => $this->t('Fade'),
      ],
      '#default_value' => isset($settings['mode']) ? $settings['mode'] : 'horizontal',
      '#title' => $this->t('Mode'),
      '#description' => $this->t('Type of transition between slides'),
    ];
    $form['general']['speed'] = [
      '#type' => 'number',
      '#title' => $this->t('Speed'),
      '#default_value' => isset($settings['speed']) ? $settings['speed'] : 500,
      '#field_suffix' => $this->t('ms'),
      '#maxlength' => 255,
      '#size' => 6,
      '#description' => $this->t('Slide transition duration (in ms)'),
    ];
    $form['general']['slideMargin'] = [
      '#type' => 'number',
      '#title' => $this->t('Slide Margin'),
      '#default_value' => isset($settings['slideMargin']) ? $settings['slideMargin'] : 0,
      '#field_suffix' => $this->t('ms'),
      '#maxlength' => 255,
      '#size' => 6,
      '#description' => $this->t('Margin between each slide'),
    ];
    $form['general']['startSlide'] = [
      '#type' => 'number',
      '#title' => $this->t('Start Slide'),
      '#default_value' => isset($settings['startSlide']) ? $settings['startSlide'] : 0,
      '#field_suffix' => $this->t('ms'),
      '#maxlength' => 255,
      '#size' => 6,
      '#description' => $this->t('Starting slide index (zero-based)'),
    ];
    $form['general']['randomStart'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Random Start'),
      '#description' => $this->t('Start slider on a random slide'),
      '#default_value' => isset($settings['randomStart']) ? $settings['randomStart'] : FALSE,
    ];

    $form['pager'] = [
      '#type' => 'details',
      '#title' => $this->t('Pager'),
      '#open' => FALSE,
    ];
    $form['pager']['pager'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Pager'),
      '#description' => $this->t('If true, a pager will be added'),
      '#default_value' => isset($settings['pager']) ? $settings['pager'] : FALSE,
    ];
    $form['pager']['pagerType'] = [
      '#type' => 'select',
      '#options' => [
        'full' => $this->t('Full'),
        'short' => $this->t('Short'),
      ],
      '#default_value' => isset($settings['pagerType']) ? $settings['pagerType'] : 'full',
      '#title' => $this->t('Pager Type'),
      '#description' => $this->t('If "full", a pager link will be generated for each slide. If "short", a x / y pager will be used (ex. 1 / 5)'),
    ];

    $form['controls'] = [
      '#type' => 'details',
      '#title' => $this->t('Controls'),

      // Controls the HTML5 'open' attribute. Defaults to FALSE.
      '#open' => FALSE,
    ];
    $form['controls']['controls'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Controls'),
      '#description' => $this->t('If true, "Next" / "Prev" controls will be added'),
      '#default_value' => isset($settings['controls']) ? $settings['controls'] : 0,
    ];
    $form['controls']['nextText'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Next Text'),
      '#default_value' => isset($settings['nextText']) ? $settings['nextText'] : 'Next',
      '#size' => 60,
      '#maxlength' => 128,
      '#description' => $this->t('Text to be used for the "Next" control'),
    ];
    $form['controls']['prevText'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prev Text'),
      '#default_value' => isset($settings['prevText']) ? $settings['prevText'] : 'Prev',
      '#size' => 60,
      '#maxlength' => 128,
      '#description' => $this->t('Text to be used for the "Prev" control'),
    ];
    $form['controls']['autoControls'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Auto Controls'),
      '#description' => $this->t('If true, "Start" / "Stop" controls will be added'),
      '#default_value' => isset($settings['autoControls']) ? $settings['autoControls'] : 0,
    ];
    $form['controls']['startText'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Start Text'),
      '#default_value' => isset($settings['startText']) ? $settings['startText'] : 'Start',
      '#size' => 60,
      '#maxlength' => 128,
      '#description' => $this->t('Text to be used for the "Start" control'),
    ];
    $form['controls']['stopText'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Stop Text'),
      '#default_value' => isset($settings['stopText']) ? $settings['stopText'] : 'Stop',
      '#size' => 60,
      '#maxlength' => 128,
      '#description' => $this->t('Text to be used for the "Stop" control'),
    ];
    $form['controls']['autoControlsCombine'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Auto Controls Combine'),
      '#default_value' => isset($settings['autoControlsCombine']) ? $settings['autoControlsCombine'] : 0,
      '#size' => 60,
      '#maxlength' => 128,
      '#description' => $this->t('When slideshow is playing only "Stop" control is displayed and vice-versa'),
    ];

    $form['auto'] = [
      '#type' => 'details',
      '#title' => $this->t('Auto'),
      '#open' => FALSE,
    ];
    $form['auto']['auto'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Auto'),
      '#description' => $this->t('Slides will automatically transition'),
      '#default_value' => isset($settings['auto']) ? $settings['auto'] : 0,
    ];
    $form['auto']['pause'] = [
      '#type' => 'number',
      '#title' => $this->t('Pause'),
      '#default_value' => isset($settings['pause']) ? $settings['pause'] : 4000,
      '#field_suffix' => $this->t('ms'),
      '#maxlength' => 255,
      '#size' => 6,
      '#description' => $this->t('The amount of time (in ms) between each auto transition'),
    ];
    $form['auto']['autoStart'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('autoStart'),
      '#description' => $this->t('Auto show starts playing on load. If false, slideshow will start when the "Start" control is clicked'),
      '#default_value' => isset($settings['autoStart']) ? $settings['autoStart'] : 1,
    ];
    $form['auto']['autoDirection'] = [
      '#type' => 'select',
      '#options' => [
        'next' => $this->t('Next'),
        'prev' => $this->t('Previous'),
      ],
      '#default_value' => isset($settings['autoDirection']) ? $settings['autoDirection'] : 'next',
      '#title' => $this->t('Auto Direction'),
      '#description' => $this->t('The direction of auto show slide transitions'),
    ];
    $form['auto']['autoHover'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('autoHover'),
      '#description' => $this->t('Auto show will pause when mouse hovers over slider'),
      '#default_value' => isset($settings['autoStart']) ? $settings['autoStart'] : 0,
    ];
    $form['auto']['autoDelay'] = [
      '#type' => 'number',
      '#title' => $this->t('Auto Delay'),
      '#default_value' => isset($settings['autoDelay']) ? $settings['autoDelay'] : 0,
      '#field_suffix' => $this->t('ms'),
      '#maxlength' => 255,
      '#size' => 6,
      '#description' => $this->t('Time (in ms) auto show should wait before starting'),
    ];

    $form['carousel'] = [
      '#type' => 'details',
      '#title' => $this->t('Carousel'),
      '#open' => FALSE,
    ];
    $form['carousel']['minSlides'] = [
      '#type' => 'number',
      '#title' => $this->t('Min Slides'),
      '#default_value' => isset($settings['minSlides']) ? $settings['minSlides'] : 1,
      '#maxlength' => 255,
      '#size' => 6,
      '#description' => $this->t('The minimum number of slides to be shown. Slides will be sized down if carousel becomes smaller than the original size.'),
    ];
    $form['carousel']['maxSlides'] = [
      '#type' => 'number',
      '#title' => $this->t('Max Slides'),
      '#default_value' => isset($settings['maxSlides']) ? $settings['maxSlides'] : 1,
      '#maxlength' => 255,
      '#size' => 6,
      '#description' => $this->t('The maximum number of slides to be shown. Slides will be sized up if carousel becomes larger than the original size.'),
    ];
    $form['carousel']['moveSlides'] = [
      '#type' => 'number',
      '#title' => $this->t('Move Slides'),
      '#default_value' => isset($settings['moveSlides']) ? $settings['moveSlides'] : 1,
      '#maxlength' => 255,
      '#size' => 6,
      '#description' => $this->t('The number of slides to move on transition. This value must be >= minSlides, and <= maxSlides. If zero (default), the number of fully-visible slides will be used.'),
    ];
    $form['carousel']['moveSlides'] = [
      '#type' => 'number',
      '#title' => $this->t('Move Slides'),
      '#default_value' => isset($settings['moveSlides']) ? $settings['moveSlides'] : 1,
      '#maxlength' => 255,
      '#size' => 6,
      '#description' => $this->t('The number of slides to move on transition. This value must be >= minSlides, and <= maxSlides. If zero (default), the number of fully-visible slides will be used.'),
    ];
    $form['carousel']['slideWidth'] = [
      '#type' => 'number',
      '#title' => $this->t('Slide Width'),
      '#default_value' => isset($settings['slideWidth']) ? $settings['slideWidth'] : 0,
      '#maxlength' => 255,
      '#size' => 6,
      '#description' => $this->t('The width of each slide. This setting is required for all horizontal carousels!'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => !empty($slider->machineName) ? $this->t('Update') : $this->t('Create'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $is_new = $form_state->getValue('is_new');
    if ($is_new) {
      $machineName = $form_state->getValue('id');
      $condition = [
        'machine_name' => $machineName,
      ];
      $slider = BXSliderModel::load($condition);
      if ($slider) {
        $form_state->setErrorByName('id', $this->t('The machine-readable name is already in use. It must be unique.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $slider = $this->slider;

    $settings = $slider->settings;
    $values = $form_state->getValues();
    foreach ($values as $setting => $value) {
      if (isset($settings[$setting])) {
        $settings[$setting] = $value;
      }
    }
    $slider->settings = $settings;
    $slider->title = $form_state->getValue('label');
    $slider->machineName = $form_state->getValue('id');
    $slider->description = '';
    $is_new = $form_state->getValue('is_new');

    $js = new SliderRender();
    $js->saveFileJs($slider->machineName, $settings);

    if ($is_new) {
      $response = $slider->saveDataSlider();
      if ($response) {
        drupal_set_message($this->t('bxSlider %slider has been created.', ['%slider' => $slider->title]));
      }
      $this->libraryDiscovery->clearCachedDefinitions();

      /* $cache = \Drupal::cache('discovery')->delete('block_plugins'); */
      $this->cacheBackendInterface->delete('block_plugins');
      $form_state->setRedirect('bxslider_block.admin.edit', [
        'slider' => $slider->machineName,
      ]);
    }
    else {
      $response = $slider->saveDataSlider($slider->bxid);
      $this->libraryDiscovery->clearCachedDefinitions();
      if ($response) {
        drupal_set_message($this->t('Slider %slider has been updated.', ['%slider' => $slider->title]));
      }
    }
  }

}
