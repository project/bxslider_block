<?php

namespace Drupal\bxslider_block\Controller;

use Drupal\bxslider_block\BXSlider;
use Drupal\bxslider_block\BXSliderModel;
use Drupal\Core\Url;
use Drupal\Core\Controller\ControllerBase;

/**
 * Class BXSliderController.
 *
 * @package Drupal\bxslider_block\Controller
 */
class BXSliderController extends ControllerBase {

  /**
   * List bx slider.
   */
  public function listbxSlider() {
    $all_slider = BXSliderModel::getAll();

    $header = [
      'slider-name' => $this->t('Slider Name'),
      'slider-description' => $this->t('Description'),
      'slider-operations' => $this->t('Operations'),
    ];
    $rows = [];
    foreach ($all_slider as $index => $slide) {
      $rows[$index]['slider-name'] = $slide->title;
      $rows[$index]['slider-description'] = $slide->description;
      $operations = [
        '#type' => 'operations',
        '#links' => [
          'config' => [
            'url' => new Url('bxslider_block.admin.configure', ['machine_name' => $slide->machine_name]),
            'title' => 'Configuration',
          ],
          'edit' => [
            'url' => new Url('bxslider_block.admin.edit', ['slider' => $slide->machine_name]),
            'title' => 'Edit ',
          ],
          'delete' => [
            'url' => new Url('bxslider_block.admin.delete', ['slider' => $slide->machine_name]),
            'title' => 'Delete',
          ],
          'clone' => [
            'url' => new Url('bxslider_block.admin.clone', ['slider' => $slide->machine_name]),
            'title' => 'Clone',
          ],
        ],
      ];
      $rows[$index]['slider-operations'] = ['data' => $operations];
    }
    return [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No any slider available. <a href="@link">Add Slider</a>.', ['@link' => Url::fromRoute('bxslider_block.admin.add')]),
      '#attributes' => ['id' => 'bx-slider'],
    ];
  }

  /**
   * Edit bx slider.
   */
  public function editbxSlider($slider) {
    $bxSlider = new BXSlider();
    $bxSlider->getDataSlider($slider);
    $form_builder = \Drupal::formBuilder();
    $build['form'] = $form_builder->getForm('\Drupal\bxslider_block\Form\BXSliderEditForm', $bxSlider);

    return $build;
  }

}
