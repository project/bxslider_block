<?php

namespace Drupal\bxslider_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\bxslider_block\BXSliderModel;
use Drupal\file\Entity\file;

/**
 * Provides blocks which belong to BX Slider.
 *
 * @Block(
 *   id = "bxslider_block",
 *   admin_label = @Translation("BX Slider"),
 *   category = @Translation("bxSlider"),
 *   deriver = "Drupal\bxslider_block\Plugin\Derivative\BXSliderBlock",
 * )
 */
class BXSliderBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $slider_name = $this->getDerivativeId();
    $entry = [
      'machine_name' => $slider_name,
    ];
    $slider = BXSliderModel::load($entry);

    if (!$slider) {
      return [
        '#markup' => "<h3>" . $this->t("This Slider doesn\'t exit") . "</h3>",
      ];
    }
    $entry = [
      'bxid' => $slider['bxid'],
    ];
    $settings = unserialize($slider['settings']);
    $slideItems = unserialize($slider['slide_items']);
    if (is_array($slideItems)) {
      foreach ($slideItems as &$value) {
        if (!empty($value['image'][0])) {
          $file = File::load($value['image'][0]);
          if ($file) {
            $value['uri'] = $file->getFileUri();
            $value['url'] = file_create_url($file->getFileUri());
          }
        }

      }
    }

    $options = [
      'bxslider'  => [$slider['bxid'] => $settings],
    ];

    return [
      '#theme' => 'bx_slider',
      '#slider_name' => $this->getDerivativeId(),
      '#bxid' => $slider['bxid'],
      '#machine_name' => $slider['machine_name'],
      '#items' => $slideItems,
      '#settings' => $settings,
      '#attached' => [
        'drupalSettings' => $options,
        'library' => [
          'bxslider_block/bxslider_block',
          'bxslider_block/bxslider_block.slider.' . $slider_name,
        ],
      ],
      '#cache' => [
        'max-age' => 0,
      ],
      '#contextual_links' => [
        'bxslider_block' => [
          'route_parameters' => ['slider' => $this->getDerivativeId()],
        ],
      ],
    ];
  }

}
